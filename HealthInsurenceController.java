package com.emids.healthinsurence;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/HealthInsurenceController")
public class HealthInsurenceController extends HttpServlet {
	
	HealthInsurenceBean healthInsurenceBean = new HealthInsurenceBean();
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//System.out.println("Inside controller class::  ");
		//response.setContentType("text/html");
		//PrintWriter out = response.getWriter();
		
	    String name = request.getParameter("name");
	    String age =request.getParameter("age");
	    String gender = request.getParameter("gender");
	    String currHelthHyperTension = request.getParameter("Hypertension");
	    String currHelthBloodPressure = request.getParameter("bloodPressure");
	    String currHelthBloodSugar = request.getParameter("bloodSugar");
	    String currHelthHOverWeight = request.getParameter("overWeight");
	    String habbitSmoking = request.getParameter("Smoking");
	    String habbitAlcohal = request.getParameter("Alcohal");
	    String habbitDailyexercise = request.getParameter("Daily exercise");
	    String habbitDrug = request.getParameter("Drugs");
	    
	    
	    request.setAttribute("name", name);
	    request.setAttribute("age", age);
	    request.setAttribute("gender", gender);
	    request.setAttribute("currHelthHyperTension", currHelthHyperTension);
	    request.setAttribute("currHelthBloodPressure", currHelthBloodPressure);
	    request.setAttribute("currHelthBloodSugar", currHelthBloodSugar);
	    request.setAttribute("currHelthHOverWeight", currHelthHOverWeight);
	    request.setAttribute("habbitSmoking", habbitSmoking);
	    request.setAttribute("habbitAlcohal", habbitAlcohal);
	    request.setAttribute("habbitDailyexercise", habbitDailyexercise);
	    request.setAttribute("habbitDailyexercise", habbitDailyexercise);
	    
	    System.out.println(request.getAttribute("name"));
        
	    
	   // RequestDispatcher rd = request.getRequestDispatcher("healthInsurence.jsp");
	    //rd.forward(request, response);
	    RequestDispatcher  rd = getServletContext().getRequestDispatcher("healthInsurence.js");
	    rd.forward(request,response);
	    System.out.println("Values are in controller :: "+name+ "  "+gender);
	   
	}
	
	//System.out.println("");

}
